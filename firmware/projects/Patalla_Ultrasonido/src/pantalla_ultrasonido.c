/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Patalla_Ultrasonido/inc/pantalla_ultrasonido.h"

#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "stdint.h"
#include "bool.h"
#include "switch.h"
#include "led.h"
#include "systemclock.h"
#include "delay.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

bool estado;			// bandera que identifica si la muestra de datos por pantalla esta activa.

uint8_t teclas;		    // varible en la que se guardara la lectura de las teclas. Puede tomar valor 1,2,4,8 dependiendo la tecla que se toque

bool cm_pg;				// bandera que se utilizara para configurar la medicion en cm cuando se encuentra en true o en pg cuando está en false

uint8_t anti_rebote;    //variable auxiliar para realizar el antirrebote

int16_t distancia;		// variable en la cual se guardará el valor de la distancia medida ya sea en cm o en pg

int16_t distancia_fija; // variable en la que se guarda la medición al momento de apretar la tecla 2, donde mientras se mantenga apretada, debe
						// mostrar siempre el mismo valor


/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/



int main(void) {
	//configuración de pines para la pantalla. Los pines nombrados son los que necesita el driver por defecto
	gpio_t pines[] = { LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5 };

	//Configuración de pines para el ultrasonido
	gpio_t echo = T_FIL2;
	gpio_t trig = T_FIL3;

	//se inicializa Switches que es para la lectura de teclas. En la misma ya configura como GPIO de entrada a los pines asociados a las teclas
	SwitchesInit();

	LedsInit(); //se inicializan los led, para que se muestre como interfaz de usuario para ver si se apreto correctamente la tecla
	            //o ver si la medida se esta realizanco en cm (LED_2 on) o en pg (LED_3 on)

	//inicialización de banderas de estado, con el valor requerido para una configuración inicial

	estado = false; 		//Se inicializa en falso para que comience a mostrar datos una vez que se toque la tecla 1

	anti_rebote=0;			//Se inicializa el antirrebote en 0, para que al tocar una telcla al comienzo del código, entre al if.

	cm_pg=true; 		    // se inicializa en true para que la configuración inicial sea en cm

	teclas=0;     			// se inicializa la variable teclas en 0

	/* se genera un bucle para que el micro no haga nada hasta que se toque la tecla 1 para prender todo
	 * luego de este while, se iniciliza la pantalla y el ultrasonido, de modo de ahorro de energía */
	while(SwitchesRead()!=SWITCH_1){

	}


	teclas=SWITCH_1; //se asigna a teclas el valor de 1 para el comienzo del programa


	SystemClockInit(); // inicialización de SystemClockInit(); lo necesita la librería del ultrasonido

    //inicialización pantalla
	ITSE0803Init(pines); //se manda como parámetro el vector de gpio con los pines

	//inicialización ultrasonido
	HcSr04Init(echo, trig); //se manda como parametros los gpio

	LedOn(LED_2); //se prende el LED2, ya que inicialmente la medición esta configurada en cm








	while (1) {
		/*=================== lectura de medición =============== */

		//lectura del sensor ultrasonido. Si cm_pg==true, mide en centimetros, sino (cm_pg==fale) mide en pulgadas
		if (cm_pg) {
			distancia = HcSr04ReadDistanceCentimeters(); //le asiga a distancia, le lectura en cm
		}
		else {
			distancia = HcSr04ReadDistanceInches(); //le asiga a distancia, le lectura en pg
		}

		/*muestra la lectura del ultrasonido por pantalla dependiendo si el sistema esta activo o no.
		  si estado==true, muestra la medicion por pantalla. Sino (estado==false) muestra un cero  */
		if(estado){
			ITSE0803DisplayValue(distancia); //muestra por pantalla el valor guardado en distancia
		}
		else {
			ITSE0803DisplayValue(0);        //muestra por pantalla un 0
		}

		/* ================== lectura de teclas ================ */

		/*si se presiona una tecla, la variable teclas debe tener un valor distinto de 0 (1,2,4,8), pero para que realmente
		  corresponda a que se encuentra presionando la tecla, la variable antirrebote debe ser igual a 0, debido que en
		  anti_rebote, se guarde el estado anterior de teclas, asignado al final del codigo. De este modo, si anti_rebote es
		  distinto de cero, quiere decir que la tecla ya se estaba presionando, y el contador de progrma no ingresa al if.
		 */
		if(teclas!=0&anti_rebote==0){
		switch (teclas) {
		//cuando se presiona la tecla 1
		case SWITCH_1:

			estado = !estado; /*cambia el estado de la variable estado. Si estado==true, muestra la medida por pantalla
			 	 	 	 	 	de lo contrario, muestra cero   */
			if(estado){
			//se encienden los 3 led asociados al boton 1 si el estado esta activo
					LedOn(LED_RGB_R);
					LedOn(LED_RGB_G);
					LedOn(LED_RGB_B);
					if(cm_pg){
						LedOn(LED_2);  //se enciende led 2 que indica medicion en cm
					}
					else{
						LedOn(LED_3);  //se enciende led 3 que indica medicion en pg
					}
			}

			else{
				//apaga todos los led por estado inactivo
				LedOff(LED_RGB_R);
				LedOff(LED_RGB_G);
				LedOff(LED_RGB_B);
				LedOff(LED_2);
				LedOff(LED_3);
			}
			break;
		//cuando se presiona la tecla 2
		case SWITCH_2:
		//	LedOn(LED_1); //se enciende Led 2 asociado a la tecla 2
			distancia_fija = distancia; 		/*se le asigna a distancia fija el valor de distancia, ya que ese valor de distancia
			 	 	 	 	 	 	 	 	 	 se mantendra inalterable hasta que se suelte el boton*/
			//mientras se mantenga apretado la tecla 2, el contador de programa no saldra del while
			while ( SwitchesRead() == SWITCH_2) { //debido a que se mantiene apretada la tecla, nunca cambiara el valor
				//si la variable estado==true, muestra el resultado por pantalla y lo mantiene constante. De lo contrario, no lo muestra
				if (estado) {
					ITSE0803DisplayValue(distancia_fija); //muestra distancia fija por pantalla
					if(!LedOn(LED_1)){
						LedOn(LED_1);
					}
				}
			}
			LedOff(LED_1); //se apaga led 2 luego de soltar el boton 2
			break;
		//cuando se presiona la tecla 3
		case SWITCH_3:
		if(estado){
		cm_pg = true; //pone la bandera cm_pg en true, por lo que se condigura la medicion en centimetros
		LedOff(LED_3); //se apaga el led 3 que indicaba la medicion en pulgadas
		LedOn(LED_2);  //se enciende led 2 que indica medicion en cm
		   }
		break;

		//cuando se presiona la tecla 4
		case SWITCH_4:
			if(estado){
		cm_pg = false; //pone la bandera cm_pg en false, por lo que se configura la medicion en pulgadas
		LedOff(LED_2); //se apaga el led 2 que indicaba la medicion en cm
		LedOn(LED_3);  //se enciende led 3 que indica medicion en pg
			}
		break;
		}

	}//cierra if de anti rebote
	anti_rebote=teclas; //le asigna a anti_rebote el valor de teclas, por lo tanto se le esta asignando el valor de lectura anterior
	//se realiza lectura de teclas
	teclas = SwitchesRead(); 		/*le asiga a la variable teclas, el valor 1,2,4,8 dependiendo si se presiona la tecla
											1,2,3,4.*/
	DelayMs(50);					//espera requerida para realizar correctamente el antirrebote


	}//cierra while(1) de bucle infinito
} //cierra el main

/*==================[end of file]============================================*/

