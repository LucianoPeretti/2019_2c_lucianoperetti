/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/ExamenPeretti.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "gpio.h"
#include "analog_io.h"
#include "bool.h"
#include "spi.h"
#include "DisplayITS_E0803.h"



/*==================[macros and definitions]=================================*/

#define NUMERO_MUESTRAS 250
/*==================[internal data definition]===============================*/


uint16_t presion_digital;		//variable donde se obtendrá el valor digital como salida del conversor, que va de 0 a 1023

uint16_t presion_fisica;		//variable donde se escalará los valores de manera lineal para que 0 equivalga a 0 y 1023 a 200[mmHg]

uint16_t maximo;				//variable donde se guarda el máximo
uint16_t promedio;				//variable donde se guarda el promedio
uint16_t minimo;				//variable donde se guarda el mínimo


uint16_t auxiliar_tiempo;		//Variable auxiliar para contar el tiempo de 1 segundo (250 muestras)

serial_config config_serial;	//estructura definida para la comunicacion serial

timer_config timer_lecura_analogica; //struct utilizado para configurar el timer

analog_input_config lectura;		//estructura para configuración y uso del ADC

bool mostrar_maximo;				//bandera para mostrar el máximo. Lo muestra con mostrar_maximo=true
bool mostrar_minimo;				//bandera para mostrar el mínimo. Lo muestra con mostrar_mínimo=true
bool mostrar_promedio;				//bandera para mostrar el promedio. Lo muestra con mostrar_mínimo=true


/*==================[internal functions declaration]=========================*/
/* Subrutina que da servicio a la interrupción del driver B que se asocia con el conversor analógico digital, que cada 4 ms (250 Hz)
 * inicia una nueva conversión*/
void ActivarConversion(void);

/*Función asociada al conversor analógico a digital, que se llama cuando termina la conversión iniciada por el timer B,
 * llama a la función AnalogInputRead de el driver del conversor, la cual actualiza la variable presion_digital que va de 0 a 1023, la
 * cual se utiliza para calcular la presión física que va de 0 (cuando presion_digital=0) a 200 (cuando presion_digital=1023)*/
void PresionAnalogica(void);

void Tecla2(void); //función que da servicio a la tecla 1.  cambia la bandera para mostrar el máximo (mostrar_maximo=true y mostrar_minimo=false) y prende el led rojo
void Tecla3(void); //función que da servicio a tecla 2. cambia la bandera para mostrar el mínimo (mostrar_minimo=true y mostrar_maximo=false) y prende el led amarillo
void Tecla4(void); //función que da servicio a la tecla 3. cambia la bandera para mostrar promedio (mostrar_minimo=false y mostrar_maximo=false) y prende el led verde

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

/*==================[internal functions definition=========================*/


void PresionAnalogica(void){
	AnalogInputRead(lectura.input,&presion_digital);								/*lee el valor digitla obtenido de la conversión AD, desde el canal CH1
	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 y lo guarda en presion digital*/
	posicion_fisica=(200/1023)*presion_digital;					//escala el valor digital a valores entre 0 y 200

	promedio=promedio+posicion_fisica;							//se suman todos los valores medidos, para luego calcular el promedio en el main.

	auxiliar_tiempo=auxiliar_tiempo+1;							//cuenta el teimpo para realizar cálculos en un segundo

}

void ActivarConversion(void){
	AnalogStartConvertion();								//inicializa la conversión analógica a digital
}

void Tecla2(void){
	LedOn(LED_1);							//enciende led
	LedOff(LED_2);							//apaga led
	LedOff(LED_3);
	mostrar_maximo=true;					//cambio de banderas
	mostrar_minimo=false;
	mostrar_promedio=false;


}

void Tecla3(void){


	Ledoff(LED_1);
	LedOn(LED_2);
	LedOff(LED_3);
	mostrar_maximo=false;
	mostrar_minimo=true;
	mostrar_promedio=false;

}

void Tecla4(void){


	LedOf(LED_1);
	LedOff(LED_2);
	LedOn(LED_3);

	mostrar_maximo=false;
	mostrar_minimo=false;
	mostrar_promedio=true;
}


int main(void)
{
	SystemClockInit();							//se inicializa el SystemClock para usar los timer


	lectura.input=CH1;							//se setea el canal en canal 1
	lectura.mode=AINPUTS_SINGLE_READ;			//se setea el modulo para manejar con un timer
	lectura.pAnalogInput=PresionAnalogica;		//se pasa la función PresionAnalogica que se llama cuando termina la conversión


	timer_lecura_analogica.timer=TIMER_B;	//se setea el timer en el B
	timer_lecura_analogica.period=4;		//se setea el tiempo del periodo en 4[ms] para una frecuencia de 250 HZ
	timer_lecura_analogica.pFunc=ActivarConversion;	//se pasa el puntero a la función que activa la conversión

	// Se inicializan las interrupciones para las 3 teclas (2,3 y 4)
	SwitchActivInt(SWITCH_2, Tecla2);
	SwitchActivInt(SWITCH_3, Tecla3);
	SwitchActivInt(SWITCH_4, Tecla4);


	config_serial.port=SERIAL_PORT_RS485; //se usara la configuración para el puerto RS485
	config_serial.baud_rate=115200;		  // velocidad de comunicacion en 115200 baudios
	config_serial.pSerial=NULL;			//No se usara una interrupción porque no se hará lectura de datos desde la pc

	UartInit(&config_serial);		//se inicializa la USART (se usará como UART)


	//configuración de pines para la pantalla. Los pines nombrados son los que necesita el driver por defecto
	gpio_t pines[] = { LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5 };

	//inicialización pantalla
	ITSE0803Init(pines); //se manda como parámetro el vector de gpio con los pines

	auxiliar_tiempo=0;				//inicializa variable para conteo de tiempo en 0
	promedio=0;						//inicializa variable para promedio en 0
	maximo=0;						//se inicializa el máximo en el valor mínimo posible por si la lectura analógica es todo 0
	minimo=200;						//se inicializa el mínimo en el valor máximo posible posible por si la lectura analógica es todo 200

	fin_segundo=false;				//Se inicializa la bandera de conteo de segundos en falso

	TimerInit(&timer_lecura_analogica);				//Se inicializan los timer con el que se establece la frecuencia de muestreo
	TimerStart(timer_lecura_analogica.timer);		//Se inicia el timmer par que comienza a contar

	LedsInit();										//Se inicializan los LED

	//se inicializa la bandera para mostroar el máximo y el mínimo en false, por lo que por defecto inicia mostrando el promedio
	mostrar_maximo=false;
	mostrar_minimo=false;



	while(1){

	/*mientras auxiliar_tiempo<251, que es equivalente a decir mientras el tiempo sea menor a 1 swgundo, se calcula el máximo, mínimo y promedio*/
   while(auxiliar_tiempo<251){
	   //Se calcula el máximo comparndo el valor actual con el valor guardado en máximo. Si es más grande sobreescribe la variable
	   if(presion_fisica>maximo){
		   maximo=presion_fisica;
	   	   	   	   	   	   	   }//fin if del máximo

	   //Se calcula el mínimo comparndo el valor actual con el valor guardado en mínimo. Si es más chico sobreescribe la variable
	   if(presion_fisica<minimo){
		minimo=presion_fisica;
	   	   	   	   	   	   	   	}//fin if minimo
   	   }//fin del while


   promedio=promedio/NUMERO_MUESTRAS;	//se calcula el promedio dividiendo por el numero de muestras
   auxiliar_tiempo=0;					//se resetea variable de tiempo

   /*Dependiendo de la tecla que toque es el valor que muestra. Por defecto muestra el primedio*/
   //muestra el maximo por pantalla LCD y serie
   if(mostrar_maximo){
		ITSE0803DisplayValue(maximo);
		UartSendString(config_serial.port ,UartItoa(maximo,10));
   }
   //muestra el minimo por pantalla LCD y serie
   else{ if(mostrar_minimo){
		ITSE0803DisplayValue(minimo);
		UartSendString(config_serial.port ,UartItoa(minimo,10));}
   	   	 else{
   	   	//muestra el promedio por pantalla LCD y serie
   	   		 if(mostrar_promedio){
   	  	ITSE0803DisplayValue(promedio);
   	  	UartSendString(config_serial.port ,UartItoa(promedio,10));
   	   		 }
   	   	 }
   }


   if(maximo>150){

	LedOn(LED_RGB_R);

   }
   else{ if(maximo<150&maximo>50){

		LedOn(LED_RGB_G);

   }
   else{
	   LedOn(LED_RGB_B);
   }

   }


	}

	}
    
	return 0;
}


/*==================[end of file]============================================*/

