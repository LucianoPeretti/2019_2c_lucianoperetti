/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "ultrasonido_timmer.h"

#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "stdint.h"
#include "bool.h"
#include "switch.h"
#include "led.h"
#include "systemclock.h"
#include "delay.h"
#include "timer.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
//declaración de variables que se utilizaran en el bucle while, para lecturas o banderas
bool estado;			// bandera que identifica si la muestra de datos por pantalla esta activa.

uint8_t teclas;		    // varible en la que se guardara la lectura de las teclas. Puede tomar valor 1,2,4,8 dependiendo la tecla que se toque
uint8_t anti_rebote=0;  //variable auxiliar para realizar el antirrebote
bool cm_pg;				// bandera que se utilizara para configurar la medicion en cm cuando se encuentra en true o en pg cuando está en false

int16_t distancia;		// variable en la cual se guardará el valor de la distancia medida ya sea en cm o en pg
int16_t distancia_fija; // variable en la que se guarda la medición al momento de apretar la tecla 2, donde mientras se mantenga apretada, debe
						// mostrar siempre el mismo valor
bool hold;				//bandera que controla la función hold. Cuando es true, se mantiene el mismo valor en pantalla

bool estado_medida;		//bandera que controla el estado de la medida, la cual es cambiada por la función de la interrupción
						//del timer, y una vez realizada la medción y mostrada, se cambia a false
timer_config timer;		//struct utilizado para configurar el timer


/*==================[internal functions declaration]=========================*/

//Declaraciones de las funciones que dan servicio a las interrupciones
//teclas
void Tecla1(void); //función que da servicio a la tecla 1. Activa y desactiva medición
void Tecla2(void); //función que da servicio a tecla 2. Hold
void Tecla3(void); //función que da servicio a la tecla 3. configura medida en cm
void Tecla4(void); //función que da servicio a la tecla 4. Configura medición en pg

//timer
void Medicion(void);//función que da servicio al timer. Cambia la bandera estado_medición


/*==================[external data definition]===============================*/



/*==================[external functions definition]==========================*/

/*==================[internal functions definitio]===========================*/

/*	Implementaciones de las funciones que dan servicio a las interrupciones		*/


/*Tecla 1: estado
 * Al tocar la tecla 1, lo unico que hace es cambiar la bandera estado.
*la bandera estado controla si el sistema esta activo (true) (realizando mediciones y mostrando por pantalla)
* o si el sistema esta inactivo (false) mostrando un cero por pantalla y sin realizar más mediciones
* Además, en el estad activo (estado==true), el timer arranca, y se para en estado inactivo (estado==false)
 */

void Tecla1(void){
	estado = !estado;			  //se cambia el estado de la variable estado
	if (estado){
		TimerStart(timer.timer); //comienza a contar el timer cuando el estado es activo
	}
	else{
		TimerStop(timer.timer);	//Se para el timer cuando el estado es inactivo
	}
}

/*Tecla 2: Hold
 * Al mantener apretada la tecla 2, se mostrara en pantalla el la medición en el instante en que se presiona.
 * Hasta que la tecla no se suelte, no se realizarán nuevas mediciones.
 * Esto se realiza copiando el valor actual de la medición en distancia_fija, y cambiando la bandera del hold, pasandola
 * a true (activa). Luego en el main, habrá un condicional para hold, y si está activo, lo cual solo es posible si no se
 * suelta la tecla 2, se mantiene dentro de un buclue en el cual se mantiene el resultado en pantalla. Luego de que se
 * suelte la tecla 2, la bandera hold pasa a falso (inactivo), y vuelve al funcionamiento habitual
*/
void Tecla2(void){
	distancia_fija = distancia; //asigna a distancia_fija el valor de distancia (medida actual)
	hold=true; 					// cambia de estado la bandera hold
}


/*Tecla 3: cm
 * Al presionar la tecla 3, se configura la medición en cm. Se realiza con la bandera cm_pg, que cuando está en
 * true, la medición es en cm, y cuando es false, la medición es en pulgadas
 * */

void Tecla3(void){
	if (estado){
	cm_pg = true; //asigna true a la bander cm_pg, para configurar la medida en cm
	LedOff(LED_3); //se apaga el led 3 que indicaba la medicion en pulgadas
	LedOn(LED_2);  //se enciende led 2 que indica medicion en cm
	}
}

/*Tecla 4: pg
 * Al presionar la tecla 4, se configura la medición en pg. Se realiza con la bandera cm_pg, que cuando está en
 * true, la medición es en cm, y cuando es false, la medición es en pulgadas
 * */
void Tecla4(void){
	if(estado){
	cm_pg = false; //asigna false a la bandera cm_pg, para configurar la medida en pg
	LedOff(LED_2); //se apaga el led 2 que indicaba la medicion en cm
	LedOn(LED_3);  //se enciende led 3 que indica medicion en pg
	}
}

/*Implementación de al función que da servicio a la interrupción del timer
 * La función lo unico que hace es poner activa la bandera estado_medida (true)
*/

void Medicion(void){
estado_medida=true;		//se asigna el valor true a la bandera estado_medida
}


int main(void) {
	//configuración de pines para la pantalla. Los pines nombrados son los que necesita el driver por defecto
	gpio_t pines[] = { LCD1, LCD2, LCD3, LCD4, GPIO1, GPIO3, GPIO5 };

	//Configuración de pines para el ultrasonido
	gpio_t echo = T_FIL2;
	gpio_t trig = T_FIL3;


	//se inicializa Switches que es para la lectura de teclas. En la misma ya configura como GPIO de entrada a los pines asociados a las teclas
	SwitchesInit();

	//se iniciañizan las interrupciones para las 4 teclas
	SwitchActivInt(SWITCH_1, Tecla1);
	SwitchActivInt(SWITCH_2, Tecla2);
	SwitchActivInt(SWITCH_3, Tecla3);
	SwitchActivInt(SWITCH_4, Tecla4);

	LedsInit(); //se inicializan los led, para que se muestre como interfaz de usuario para ver si se apreto correctamente la tecla o ver si la medida
				  // se esta realizanco en cm o en pg

	estado_medida=false;	//se inicializa la bandera estado_medida en false porque todavía no
							//arranco el timer

	/*Configuración del timer. Se utilizara el timer A, con un periodo de 1[s]
	 * */

	timer.timer=TIMER_A;	//se setea el timmer
	timer.period=1000;		//se setea el tiempo del periodo en [ms]
	timer.pFunc=Medicion;	//se pasa el puntero a función de la medición (da servicio al timer)

	TimerInit(&timer);		//Se inicializa el timer, pasandole como parametro el struc para configuración




	estado = false; 		//Se inicializa en falso para que comience a mostrar datos una vez que se toque la tecla 1

	/* se genera un bucle para que el micro no haga nada hasta que se toque la tecla 1 y cambie el estado para prender todo
			 * luego de este while, se iniciliza la pantalla y el ultrasonido, de modo de ahorro de energía */
	while(!estado){

	}

	SystemClockInit(); // inicialización de SystemClockInit(); lo necesita la librería del ultrasonido

	TimerStart(timer.timer); //se dispara el timer por primera vez

	//inicialización pantalla
	ITSE0803Init(pines); //se manda como parámetro el vector de gpio con los pines

	//inicialización ultrasonido
	HcSr04Init(echo, trig); //se manda como parametros los gpio


	cm_pg=true; 		    // se inicializa en true para que la configuración inicial sea en cm

	hold=false; 			//por defecto, el estado hold esta inactivo

	LedOn(LED_2);


	while (1) {

			if(estado){
				LedOn(LED_RGB_R);
				LedOn(LED_RGB_G);
				LedOn(LED_RGB_B);
				LedOn(LED_2);
							//si el hold esta inactivo, se realizan mediciones y se muestran por pantalla
					if(!hold){
						if(estado_medida){
							LedOn(LED_1);//se usa led 1 como interfaz de parpadeo, para mostrar que rebalso el timer
						//realiza medición en cm
						if (cm_pg) {
							distancia = HcSr04ReadDistanceCentimeters();

						}
						//realiza medición en pg
						else {
							distancia = HcSr04ReadDistanceInches();

						}
						//fin de medición

						ITSE0803DisplayValue(distancia); //se muestra por pantalla la medición realizada
						}
						LedOff(LED_1);
						estado_medida=false;
					}
					//caso de hold activado. Se mantendra dentro del while hasta que se suelte la tecla
					else{
						while(SwitchesRead() == SWITCH_2){
							if(!LedOn(LED_1)){
								LedOn(LED_1);// enciende led 1
							}
							if(estado_medida){
							ITSE0803DisplayValue(distancia_fija); //muestra por pantalla distancia fija
							}
							estado_medida=false;
						}
						// se libera la tecla, por lo que se apaga el led 1 y se desactiva el hold
						LedOff(LED_1);
						hold=false;
					}
			}
			//estado inactivo (variable estado==false) Muestra valor 0 y se apagan los Leds
			else {
				ITSE0803DisplayValue(0);
				LedOff(LED_RGB_R);
				LedOff(LED_RGB_G);
				LedOff(LED_RGB_B);
				LedOff(LED_2);
				LedOff(LED_3);
				cm_pg=true; //se vuelve a su valor por defecto
			}
		}

	return 0;
}

/*==================[end of file]============================================*/


