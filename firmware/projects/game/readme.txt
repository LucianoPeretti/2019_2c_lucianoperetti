- Proyecto Game

Juego de evasión de obstáculos. Se muestra en Display LCD color ili9341 y se controla por potenciómetro asociado.
Los obstáculos se acercan y la pelota debe pasar por la abertura, sin tocar los bordes.
Para reiniciar el juego se debe presionar RESET.


Conexión para lectura analógica por potenciómetro: 
Punto medio de pote a Pin11 (CH2) Header 1 de placa
Un extremo a Pin17 Header 1 (VDDA)
Otro extremo a Pin12 Header 1 (GNDA)

Conexiones de Display ili9341 a EDU-CIAA-NXP LPC 4337:
Pin1 (Vcc) se puentea a Pin8 (LED) y se conectan a Pin1 Header2 de placa (3.3V)
Pin2 (GND) se conecta a Pin3 Header2 de placa (GND)
Pin3 (CS) se conecta a Pin32 Header2 de placa (GPIO1)
Pin4 (RESET) se conecta Pin36 Header2 de placa (GPIO5)
Pin5 (DC) se conecta Pin38 Header2 de placa (GPIO3)
Pin6 (SDI) se conecta Pin21 Header2 de placa (SPI_MOSI)
Pin7 (SCK) se conecta a Pin20 Header 2 de placa (SPI_SCK)
Pin9 (SDO/MISO) se conecta a Pin18 Header 2 de placa (SPI_MISO)



