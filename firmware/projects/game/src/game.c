/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/game.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "ili9341.h"
#include "timer.h"
#include "gpio.h"
#include "analog_io.h"
#include "bool.h"
#include "spi.h"
#include "fonts.h"


/*==================[macros and definitions]=================================*/

#define RADIO_PELOTA 10					//radio de la pelota
#define POSICION_X_PELOTA 70	   	    //posición x de la pelota. Se mantendra siempre en el mismo lugar
#define POSICION_Y_PELOTA_INICIAL 120	//posición y inicial, donde estará al inicio.
#define ANCHO_HENDIDURA 40				//ancho hendidura
#define ALTO_HENDIDURA 40				//alto hendidura
#define ANCHO_PANTALLA 240				//ancho total de la pantalla
#define VALOR_MAXIMO_ANALOGICO 1023		//valor máximo de discriminación de los valores analógicos .
#define POSICION_INICIO_OBSTACULO 240   /*se defina la posicion inicial del obstáculo en la posicion 240 para la parte izquierda, por lo que la
										/parte derecha inicia en la posicion 280, para que no pise el puntaje */
#define POSICION_INICIAL_COMPARACION 80	//posición de inicio de comparación
#define POSICION_FINAL_COMPARACION 20	//posicion final de comparación
#define BORRADO_EXTRA 15				/*necesario para borrar cuando el objeto llega al final de la pantalla, ya que al aumentar su velocidad, aveces no se
										borraba el obstáculo completo*/
/*==================[internal data definition]===============================*/

uint16_t posicion_y_pelota;	 	    //posición y de la pelota. Será actualizada dependiendo de la entrada analógica
uint16_t posicion_x_obstaculo;		//posición x del obstáculo, que se irá actualizando para moverlo

uint16_t movimiento;				//variable que guarda de a cuantos píxeles se van moviendo los obstaculos. Esta se aumenta con la
									//aparición de cada obstaculo

timer_config timer_obstaculos;		//struct utilizado para configurar el timer de los obstáculos (timer A)
timer_config timer_pelota;			//struct utilizado para configurar el timer de la pelota	  (timer B)

gpio_t entrada_analogica;			//variable para lectura de la entrada analógica

analog_input_config lectura;		//estructura para configuración y uso del ADC

uint16_t lectura_analogica;			//Valor que se va modificando con la entrada analógica

bool estado_juego;				   // variable de estado que controla si se continúa el juego (true) o si perdió (false)

uint16_t posicion_hendidura;		// posición del vértice superior izquierdo de la hendidura. Se va actualizando para cada obstáculo

bool fin_barra;						//bandera establece cuando el obstáculo llega al final (false). Antes de llegar al final se encuentra en true


/*==================[internal functions declaration]=========================*/
/*Función que dibuja la pelota. Primero borra la pelota, con la posición anterior, con el color del fondo, y luego
 * dibuja con la posición actual, que se recibe por parámetro. La posición anterior se guarda en una variable
 * estática dentro de la función. La función será llamada en cada ciclo del timer de la pelota.
 * Se aclara que la posición x de la pelota no es modificada nunca*/
void DibujarCirculo(uint16_t y);


/*Función que dibuja los obstáculos. Primero borra la parte posterior del obstáculo, y luego se dibuja según la posición
 * actual. La misma se recibe por parámetro, el punto (x0,y0) corresponde al vértice superior izquierdo de la hendidura.
 * como el ancho y el alto de la misma es fijo, con estos dos datos ya puede graficarse todoel obstáculo*/
void DibujarObstaculo(uint16_t x0, uint16_t y0);

/*Función que da servicio a la interrupción del timmer A, la cual actualiza la posición de los obstáculos
 * Moviendolos hacia la izquierda. Además cuando el obstáculo llega al final de la pantalla, camiba la bandera
 * fin_barra, poniendola en false, saliendo de un bucle while, actualizando los parámetros para un nuevo obstáculo*/

void Desplazamiento(void);

/*Función que da servicio a la interrupción del timer B, la cual está asociada con la lectura analógica que actualiza
 * la posición y de la pelota. Lo que realiza esta función es iniciar la conversión analógica */
void PosicionYPelota(void);

/*Función asociada al conversor analógico a digital, que se llama cuando termina la conversión iniciada por el timer B,
 * llama a la función AnalogInputRead de el driver del conversor, la cual actualiza la variable lectura_analogica, la
 * cual se utiliza para calcular la nueva posición y de la pelota, guardada en posicion_y_pelota*/
void LecturaAnalogica(void);

/*Función de finalización del juego. Esta función es llamada cuando el obstáculo esta pasando por la posición x
 * de la pelota, y compara la posición y ancho de la hendidura con la posición y ancho de la pelota. Si estas se
 * interpone, camia la bandera estado_juego, poniendo en false, lo que determina el fin del juego */
void GameOver(uint16_t altura_pelota, uint16_t altura_obstaculo);

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

/*==================[internal functions definition=========================*/

void DibujarCirculo( uint16_t y){
static uint16_t posicion_y_anterior;														/*variable estática para guardar la posición anterior de la pelota.
 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 No se borra cuando termina la función	*/
ILI9341DrawFilledCircle(POSICION_X_PELOTA,posicion_y_anterior,RADIO_PELOTA,ILI9341_CYAN);	/*borra la pelota en la posición anterio, dibujandola con el color del
 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 fondo*/
ILI9341DrawFilledCircle(POSICION_X_PELOTA,y,RADIO_PELOTA,ILI9341_RED);						/*dibuja la pelota en su posición actual*/
posicion_y_anterior=y;																		//se actualiza posicion_y_anterior que guarda la posición y anterior
}

void DibujarObstaculo(uint16_t x0, uint16_t y0){
	ILI9341DrawFilledRectangle(x0,0,x0+ANCHO_HENDIDURA,y0,ILI9341_GREEN);										//dibuja el obstáculo de arriba
	ILI9341DrawFilledRectangle(x0,y0+ANCHO_HENDIDURA,x0+ANCHO_HENDIDURA,ANCHO_PANTALLA,ILI9341_GREEN);						//dibuja el obstáculo de abajo
	ILI9341DrawFilledRectangle(x0+ANCHO_HENDIDURA,0,x0+ANCHO_HENDIDURA+movimiento,y0,ILI9341_CYAN);				//borra el obstáculo de arriba en la posición anterior
	ILI9341DrawFilledRectangle(x0+ANCHO_HENDIDURA,y0+ALTO_HENDIDURA,x0+ANCHO_HENDIDURA+movimiento,ANCHO_PANTALLA,ILI9341_CYAN);//borra el obstáculo de abajo en la posición anterior
	}

void Desplazamiento(void){
	if(posicion_x_obstaculo-movimiento>0){							//condicional que da false cuando el obstáculo llega al final de la pantalla
	posicion_x_obstaculo=posicion_x_obstaculo-movimiento;			//Si no llego al final de la pantalla actualiza la posición x del obstáculo
	}
	else {
		fin_barra=false;											//Si llego al final de la pantalla, actualiza la bandera fin_barra (false)
	}

}

void PosicionYPelota(void){
	AnalogStartConvertion();								//inicializa la conversión
}

void LecturaAnalogica(void){
	AnalogInputRead(lectura.input,&lectura_analogica);								/*lee el valor digitla obtenido de la conversión AD, desde el canal CH2
	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 y lo guarda en lectura analógica*/
	posicion_y_pelota=(ANCHO_PANTALLA*lectura_analogica)/VALOR_MAXIMO_ANALOGICO;    /*realiza el escalado entre los 1024 valores digitales del conversor
																					y los 240 pixeles de nuestra pantalla */
}


void GameOver(uint16_t altura_pelota, uint16_t altura_obstaculo){
	if(((altura_pelota-RADIO_PELOTA)<altura_obstaculo)|((altura_pelota+RADIO_PELOTA)>(altura_obstaculo+ALTO_HENDIDURA))){   /*si la pelota choca un objeto, entra al
	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 	 if y cambia la bandera que finaliza el jeugo*/
		estado_juego=false;
	}
}


int main(void)
{

	SystemClockInit();
	//Se inicializa el timer, pasandole como parametro el struc para configuración
	timer_obstaculos.timer=TIMER_A;	//se setea el timmer
	timer_obstaculos.period=100;		//se setea el tiempo del periodo en [ms]
	timer_obstaculos.pFunc=Desplazamiento;	//se pasa el puntero a función de desplazamiento (da servicio al timer)

	//Se inicializa el timer, pasandole como parametro el struc para configuración

	timer_pelota.timer=TIMER_B;	//se setea el timer
	timer_pelota.period=100;		//se setea el tiempo del periodo en [ms]
	timer_pelota.pFunc=PosicionYPelota;	//se pasa el puntero a función de posición y de la pelota (da servicio al timer)

	//se inicializa el ADC, pasandole como parámetro el struct para configuración

	lectura.input=CH2;							//se setea el canal en canal 2
	lectura.mode=AINPUTS_SINGLE_READ;			//se setea el modulo para manejar con un timer
	lectura.pAnalogInput=LecturaAnalogica;		//se pasa la función Lectura_Analogica que se llama cuando termina la conversión

	estado_juego=true;							//se inicializa el estado_juego en true
	uint32_t puntaje=0;							//se incializa el puntaje en 0
	ILI9341Init(SPI_1,GPIO1,GPIO3,GPIO5);		//se inializa la pantalla pasando como parámetro los GPIO y el puerto SPI usado

	ILI9341Rotate(ILI9341_Landscape_1);												//se rota la pantalla para usarla apaisada (horizontal)
	ILI9341Fill(ILI9341_CYAN);														//se pinta el fondo de color cyan
	ILI9341DrawFilledRectangle(290,0,320,20,ILI9341_BLACK);							//se pinta rectangulo negro para el puntaje
	ILI9341DrawInt(295, 0, puntaje, 2, &font_11x18, ILI9341_WHITE, ILI9341_BLACK);	//se escribe el puntaje, inicialmente con valor 0

	DibujarCirculo(POSICION_Y_PELOTA_INICIAL);										//se dibuja la pelota en la posicion inicial, a la mitad de la pantalla en y, y en
																					//su posicion unica en x.

	AnalogInputInit(&lectura);					//Se inicializa el conversos AD, pasandole por referencia el struc de configuracion

	TimerInit(&timer_pelota);					//Se inicializa el timer B asociado a la lectura analógica
	TimerInit(&timer_obstaculos);				//Se inicializa el timer A que llama a la función que desplaza los obstáculso
	TimerStart(timer_pelota.timer);				//Comienza a contar el timer B
	TimerStart(timer_obstaculos.timer);			//Comienza a contar el timer A

	posicion_x_obstaculo=POSICION_INICIO_OBSTACULO;  //se inicializa la variable posicion x del obstáculo con la posición inicial

	fin_barra=true;		// se inicializa en true la bandera fin_barra, que cuando está en false indica que el obstáculo llego al final. Se modifica en Desplazamiento


	/*Se crea un vector de obstáculos "pseudoaleatorio", donde se ponene 20 valores para el vértice superior izquierdo de la hendidura. Con
	 * este valor, ya es posible calcular el resto, ya que el ancho y alto de la hendidura son fijos*/
	uint16_t vector_obstaculo[]={100,10,90,25,40,120,60,99,130,15,170,47,100,10,133,200,14,99,67,12,200};

	uint8_t i=0;		//variable auxiliar utilizada para recorrer el vector de obstáculos

	movimiento=5;		/*se inicializa la variable movimiento con 5. Esta luego de cada obstáculo se aumenta en una unidad, aumentando la velocidad
						del juego*/

	//Comienzo del bucle repetitivo

	while(1){
	//El programa se encontrará en este bucle while hasta que el juegador pierda. La bandera estado_juego se modifica en la función GameOver
		while(estado_juego){
			posicion_hendidura=vector_obstaculo[i];					//se define la posición de la hendidura de cada obstáculo

			//El programa se mantendrá en este while hasta que la barra llegue al final de la pantalla o el jugador pierda
			while(fin_barra&estado_juego){
				DibujarCirculo(posicion_y_pelota);				//se dibuja la pelota, y se va actualizando su posición y con el timer y el conversor AD
				DibujarObstaculo(posicion_x_obstaculo,posicion_hendidura);	//se dibuja el obstáculo, que se va actualizaando su posición y con el timer

				/*se ingresa a este if cuando el obstáculo está pasando por una posición donde puede entrar en contacto con la pelota.
				 * Los valores de POSICION_INICIAL_COMPARACION y POSICION_FINAL_COMPARACION tienen en cuenta el ancho de la pelota y del obstáculo*/
				if((posicion_x_obstaculo<POSICION_INICIAL_COMPARACION)&(posicion_x_obstaculo>POSICION_FINAL_COMPARACION)){
					GameOver(posicion_y_pelota, posicion_hendidura);		//se manda la posición y de la pelota y hendidura para comparación
				} //fin del if
			}//fin del while de la barra

			//el obstáculo llego al final de la pantalla y el usuario no perdio
			posicion_x_obstaculo=POSICION_INICIO_OBSTACULO;					//se actualiza la posición del obstáculo pq llego al final
			ILI9341DrawFilledRectangle(0,0,ANCHO_HENDIDURA+BORRADO_EXTRA,ANCHO_PANTALLA,ILI9341_CYAN);	//se borra el obstáculo al llegar al final
			fin_barra=true;							//se actualiza bandera de fin de barra para un nuevo nivel (nuevo obstáculo)
			puntaje = puntaje +1;					//se suma uno al puntaje
			ILI9341DrawInt(295, 0, puntaje, 2, &font_11x18, ILI9341_WHITE, ILI9341_BLACK);		//se dibuja continuamente el score
			i=i+1;						//se suma uno a la variable de control de vector de obstáculos
			movimiento=movimiento+1;	//se aumenta en 1 la valocidad (los obstáculos se mueven de a 1 pixel más)

		}// fin del while estado_juego

		//FIN DEL JUEGO. El uusario perdió

		ILI9341Fill(ILI9341_BLACK);		//se pinta el fondo de negro

		//Se crea un bucle infinito para que el programa quede colgado mostrando la pantalla final y no siga intentando entrar al resto del programa
		while(1){

		ILI9341DrawString(60, 100, "Perdiste JAJA", &font_16x26, ILI9341_WHITE, ILI9341_BLACK);	//se escribe mensaje de fin de juego
		//se comunica el puntaje obtenido
		ILI9341DrawString(60, 150, "  Puntaje:  ", &font_16x26, ILI9341_WHITE, ILI9341_BLACK);
		ILI9341DrawInt(240, 150, puntaje, 2, &font_16x26, ILI9341_WHITE, ILI9341_BLACK);
		}//fin del while del juego perdido
	}	//fin del bucle repetitivo
    
	return 0;
}//fin del main


/*==================[end of file]============================================*/

