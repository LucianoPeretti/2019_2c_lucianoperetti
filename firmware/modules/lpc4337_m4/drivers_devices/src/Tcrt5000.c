/* @brief  EDU-CIAA NXP GPIO driver
 *
 *
 * This driver provide functions to generate delays using Timer0
 * of LPC4337
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/11/2018 | Document creation		                         |
 *
 */

#include "Tcrt5000.h"

/*****************************************************************************
 * Private macros/types/enumerations/variables definitions
 ****************************************************************************/

gpio_t driver_state;

/*****************************************************************************
 * Public types/enumerations/variables declarations
 ****************************************************************************/

/*****************************************************************************
 * Private functions definitions
 ****************************************************************************/

/*****************************************************************************
 * Private functions declarations
 ****************************************************************************/

/*****************************************************************************
 * Public functions declarations
 ****************************************************************************/

bool Tcrt5000Init(gpio_t dout)
{

	driver_state = dout;
	GPIOInit(driver_state, GPIO_INPUT);
	return (1);
}
bool Tcrt5000State(void)
{
	return(GPIORead(driver_state));
}
bool Tcrt5000Deinit(gpio_t dout)
{
	GPIODeinit();
	return(1);
}

