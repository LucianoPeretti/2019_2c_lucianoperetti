/* @brief  EDU-CIAA NXP GPIO driver
 *
 *
 * This driver provide functions to generate delays using Timer0
 * of LPC4337
 *
 * @note
 *
 * @section changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/11/2018 | Document creation		                         |
 *
 */

#ifndef TCRT5000_H_
#define TCRT5000_H_

#include <stdint.h>
#include "gpio.h"

/*****************************************************************************
 * Public macros/types/enumerations/variables definitions
 ****************************************************************************/

/*****************************************************************************
 * Public functions definitions
 ****************************************************************************/

/**
 * @brief Init driver
 * @param[in] gpio dout
 * @return TRUE if no error
 */
bool Tcrt5000Init(gpio_t dout);

/**
 * @brief Driver state
 * @param[in] No parameter
 * @return Bool indicating driver state
 */
bool Tcrt5000State(void);

/**
 * @brief Deinit driver
 * @param[in] gpio dout
 * @return TRUE if no error
 */
bool Tcrt5000Deinit(gpio_t dout);


#endif /* TCRT5000_H_ */
