/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*Ejercicio*/
/* B) Defina un vector de estructuras llamado menuPrincipal. Cada estructura cuenta con un campo txt
del tipo char que contiene la etiqueta de la opción de menú, y un campo doit qué es un puntero a una
función a ejecutar al seleccionar este ítem del menú
typedef struct
{
char * txt ;  Etiqueta de la opción
void (*doit)() ;  Función a ejecutar en caso de seleccionar esa opción
} menuEntry ;
menuEntry menuPrincipal [] =
{
{ "etiqueta" , punt_a_función},
…….
};
Escriba una función que reciba como parámetro un puntero al vector la opción de menú a ejecutar y,
empleando punteros a función ejecute.
EjecutarMenu(menuEntry * menu , int option)
{} */

/*		Defino la estructura		*/



typedef struct{
	char *txt;		/*		etiqueta de la opcion		*/
	void (*doit)(); /*		funcion a ejecutar en caso de seleccionar opcion*/

			   } menuEntry;

void EjecutarMenu(menuEntry *menu, int opcion){
	/*if(opcion==1){
		menu[0].doit();
	}
	else{
		if(opcion==2){
		menu[1].doit();
	}
		else{if(opcion==3){
			menu[2].doit();
		}

		}
	}*/
	menu[opcion-1].doit();//el menos 1 es para que el usuario ponga del 1 al 3
}

void Funcion1(){
	printf("ejecutando funcion 1\r\n");
}

void Funcion2(){
	printf("ejecutando funcion 2\r\n");
}

void Funcion3(){
	printf("ejecutando funcion 3\r\n");
}

menuEntry menuPrincipal[3]={{"funcion 1",Funcion1}, {"funcion 2",Funcion2},{"funcion 3",Funcion3}};


int main(void)
{
EjecutarMenu(menuPrincipal,1);
EjecutarMenu(menuPrincipal,2);
EjecutarMenu(menuPrincipal,3);

		return 0;
}



/*==================[end of file]============================================*/

