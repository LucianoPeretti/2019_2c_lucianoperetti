/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*Ejercicio*/
/*C) Escriba una función que reciba un dato de 32 bits, la cantidad de dígitos de salida y un puntero a
un arreglo donde se almacene los n dígitos. La función deberá convertir el dato recibido a BCD,
guardando cada uno de los dígitos de salida en el arreglo pasado como puntero.
BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{} */

/*void BinaryToBcd(uint32_t data, uint8_t digits, uint8_t *bcd_number){


}

*/

void BinaryToBcd(uint32_t data, uint8_t digits, uint8_t *bcd_number){
	uint8_t i=0;
	while(i<digits){
	bcd_number[digits-i-1]=data%10;
	data=data/10;
	//printf("%d",bcd_number[i]);
	i++;
	}
}

int main(void)
{
uint8_t bcd[5],i;
BinaryToBcd(98765,5,bcd);
for(i=0;i<5;i++){
	printf("el dato %d es %d\r\n",i,bcd[i]);

}


	return 0;
}

/*==================[end of file]============================================*/

