/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*Ejercicio*/
/*D) Escribir una función que reciba como parámetro un dígito BCD y un vector de estructuras del tipo
gpioConf_t.
typedef struct
{
uint8_t port; !< GPIO port number
uint8_t pin; !< GPIO pin number
uint8_t dir; !< GPIO direction ‘0’ IN; ‘1 ’ OUT
} gpioConf_t;
Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14
La función deberá establecer en qué valor colocar cada bit del dígito BCD e indexando el vector
anterior operar sobre el puerto y pin que corresponda.

*/

typedef struct {
	uint8_t port; //numero de puerto
	uint8_t pin;  //numero de pin
	uint8_t dir;  // direccion 0 para in y 1 para 0
} gpioConf_t;

void Control(uint8_t bcd,gpioConf_t *vector){
	uint8_t i,aux;
	for(i=0;i<4;i++){
		aux=bcd&1;
		if(aux==0){
			printf("en b%d hay un 0\r\n",i);
		}
		else{printf("en b%d hay un 1\r\n",i);}
		bcd=(bcd>>1);
	}

}

int main(void)
{
	gpioConf_t puertos[4];
	//primer puerto
	puertos[0].port=1;
	puertos[0].pin=4;
	puertos[0].dir=1;
	//segundo puerto
	puertos[1].port=1;
	puertos[1].pin=5;
	puertos[1].dir=1;
	//tercer puerto
	puertos[2].port=1;
	puertos[2].pin=6;
	puertos[2].dir=1;
	//cuarto puerto
	puertos[3].port=2;
	puertos[3].pin=14;
	puertos[3].dir=1;

	Control(9,puertos);

	return 0;
}

/*==================[end of file]============================================*/

