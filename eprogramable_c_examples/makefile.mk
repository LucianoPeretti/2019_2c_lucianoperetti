########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

#PROYECTO_ACTIVO = prueba
#NOMBRE_EJECUTABLE = prueba.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio01
#NOMBRE_EJECUTABLE = Guia1_Ejercicio01.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio02
#NOMBRE_EJECUTABLE = Guia1_Ejercicio02.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio03
#NOMBRE_EJECUTABLE = Guia1_Ejercicio03.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio04
#NOMBRE_EJECUTABLE = Guia1_Ejercicio04.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio05
#NOMBRE_EJECUTABLE = Guia1_Ejercicio05.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio06
#NOMBRE_EJECUTABLE = Guia1_Ejercicio06.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio07
#NOMBRE_EJECUTABLE = Guia1_Ejercicio07.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio08
#NOMBRE_EJECUTABLE = Guia1_Ejercicio08.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio09
#NOMBRE_EJECUTABLE = Guia1_Ejercicio09.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio10
#NOMBRE_EJECUTABLE = Guia1_Ejercicio10.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio11
#NOMBRE_EJECUTABLE = Guia1_Ejercicio11.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio12
#NOMBRE_EJECUTABLE = Guia1_Ejercicio12.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio14
#NOMBRE_EJECUTABLE = Guia1_Ejercicio14.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio16
#NOMBRE_EJECUTABLE = Guia1_Ejercicio16.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio17_18
#NOMBRE_EJECUTABLE = Guia1_Ejercicio17_18.exe

#PROYECTO_ACTIVO = Guia1_Ejercicio17_21
#NOMBRE_EJECUTABLE = Guia1_Ejercicio17_21.exe

#PROYECTO_ACTIVO = Guia1_IntegradorA
#NOMBRE_EJECUTABLE = Guia1_IntegradorA.exe

#PROYECTO_ACTIVO = Guia1_IntegradorB
#NOMBRE_EJECUTABLE = Guia1_IntegradorB.exe

#PROYECTO_ACTIVO = Guia1_IntegradorC
#NOMBRE_EJECUTABLE = Guia1_IntegradorC.exe

PROYECTO_ACTIVO = Guia1_IntegradorD
NOMBRE_EJECUTABLE = Guia1_IntegradorD.exe