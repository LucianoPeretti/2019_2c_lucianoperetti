/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Guia1_Ejercicio17_18/inc/main.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*Ejercicio*/
/*1 7. Realice un programa que calcule el promedio de los 15 números listados abajo, para ello,
primero realice un diagrama de flujo similar al presentado en el ejercicio 9. (Puede utilizar la
aplicación Draw.io). Para la implementación, utilice el menor tamaño de datos posible:
234 123 111 101 32
116 211 24 214 100
124 222 1 129 9
===============================================================================
18. Al ejercicio anterior agregue un número más (1 6 en total), modifique su programa de manera
que agregue el número extra a la suma e intente no utilizar el operando división “/”
N°1 6 =>233. Si utiliza las directivas de preprocesador ( #ifdef, #ifndef,#endif, etc) no necesita
generar un nuevo programa.
 */


int main(void)
{
#define PROM16
#ifdef PROM15

   uint8_t a[15]={234,123,111,101,32,116,211,24,214,100,124,222,1,129,9};
   uint16_t promedio=0;
   uint8_t i;
   for(i=0;i<sizeof(a);i++){
	   promedio=promedio+a[i];
	      }
   promedio=promedio/sizeof(a);
   printf("el promedio es: %d ",promedio);
#endif
#ifdef PROM16
   uint8_t a[16]={234,123,111,101,32,116,211,24,214,100,124,222,1,129,9,233};
      uint16_t promedio=0;
      uint8_t i;
      for(i=0;i<sizeof(a);i++){
   	   promedio=promedio+a[i];
   	      }
     promedio=(promedio>>4);
     printf("el promedio es: %d ",promedio);
#endif
	return 0;
}

/*==================[end of file]============================================*/

