/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*Ejercicio*/
/*A) Realice un función que reciba un puntero a una estructura LED como la que se muestra a
continuación:
struct leds
{
uint8_t n_led; indica el número de led a controlar
uint8_t n_ciclos; indica la cantidad de cilcos de encendido/apagado
uint8_t periodo; indica el tiempo de cada ciclo
uint8_t mode; ON, OFF, TOGGLE
}my_leds, */

#define ON 1
#define OFF 2
#define TOOGLE 3

typedef struct{
	uint8_t n_led; 		  //indica el numero de led a controlar
	uint8_t n_ciclos;     //Indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;  	  //indica el tiempo de cada ciclo
	uint8_t mode; 		  // ON, OFF, TOOGLE
	} my_leds;


void ControlLed(my_leds *LED){
//==================  MODO ON  ==========================
	if((*LED).mode==ON){
		  if((*LED).n_led==1){
			/*		insertar codigo de manejo de led 	 */
			printf("Enciende LED 1 \r\n");
		}
		  else {if((*LED).n_led==2){
			/*		insertar codigo de manejo de led 	 */
						printf("Enciende LED 2 \r\n");
		}
		        else{ if((*LED).n_led==3){
			  /*		insertar codigo de manejo de led 	 */
						printf("Enciende LED 3 \r\n");
					}
				}//fin else 2
		}//fin else 1
	}//fin if modo on

//==================  MODO OFF  ==========================
	else{ if((*LED).mode==OFF){
				if((*LED).n_led==1){
					/*		insertar codigo de manejo de led 	 */
					printf("Apaga LED 1 \r\n");
				}
				else {if((*LED).n_led==2){
							/*		insertar codigo de manejo de led 	 */
								printf("Apaga LED 2 \r\n");
				}
						else{ if((*LED).n_led==3){
								/*		insertar codigo de manejo de led 	 */
								printf("Apaga LED 3 \r\n");
		     		                             }
				           }//fin else 2
			        }//fin else 1
		                      }//fin if modo OFF

//==================  MODO TOOGLE  ==========================
	     else{ if((*LED).mode==TOOGLE){

	    	 uint8_t i=0;//variable auxiliar para recorrer los ciclos
	    	 uint8_t j=0;//variable auxiliar para el retardo
	    	 while(i<(*LED).n_ciclos){
	    		  if((*LED).n_led==1){
	    			  /*	insertar código de tootle de led	  */
	    			  printf("se realiza el toogle de led 1 \r\n");
	    		  }
	    		  else{ if((*LED).n_led==2){
	    			  /*	insertar código de tootle de led	  */
	    			  printf("se realiza el toogle de led 2 \r\n");
	    			  	}
	    		  	    else{ if((*LED).n_led==3){
	    		  	      /*	insertar código de tootle de led	  */
	    		  	     printf("se realiza el toogle de led 3 \r\n");
	    		  	          }
	    		  	       }//fin else 2
	    		       } //fin else 1
	    		i++;
	    		while(j<(*LED).periodo){
	    			/*		insertar código de manejo de retardo		*/
	    			j++;
	    		}
	    		printf("se espera %d milisegundos", (*LED).periodo);
	    		j=0;
	    	                          }//fin del while
	                               } //fin if modo TOOGLE
	        }//fin del else modo TOOGLE
	  } //fin else del comienzo del modo off

                  } //fin de la funcion


int main(void)
{
my_leds *LED1;
(*LED1).mode=TOOGLE;
(*LED1).n_led=1;
(*LED1).n_ciclos=30;
(*LED1).periodo=200;
ControlLed(LED1);


	return 0;
}

/*==================[end of file]============================================*/

