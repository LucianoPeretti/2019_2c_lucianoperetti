/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../Guia1_Ejercicio14/inc/main.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

/*Ejercicio*/
/*11 4. Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres, “apellido” de 20
caracteres y edad.
a. Defina una variable con esa estructura y cargue los campos con sus propios datos.
b. Defina un puntero a esa estructura y cargue los campos con los datos de su
compañero (usando acceso por punteros).*/

typedef struct {
	char nombre[10];
	char apellido[20];
	uint8_t edad;
} alumno;

int main(void)
{
	alumno LP,*JP;
	strcpy(LP.nombre,"Luciano");
	strcpy(LP.apellido,"Peretti");
	LP.edad=22;
	printf("nombre %s\n\r",LP.nombre);
	printf("apellido %s\n\r",LP.apellido);
	printf("edad %d\n\r",LP.edad);
	strcpy((*JP).nombre,"Justo");
	strcpy((*JP).apellido,"Princich");
	(*JP).edad=21;
	printf("Nombre: %s\n\r",(*JP).nombre);
	printf("Apellido: %s\n\r",(*JP).apellido);
	printf("Nombre %d\n\r",(*JP).edad);

	return 0;
}

/*==================[end of file]============================================*/

